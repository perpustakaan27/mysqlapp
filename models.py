from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String

Base = declarative_base()
engine = create_engine('mysql+mysqlconnector://root:thegreatisfriday@localhost:3306/rentalfilm', echo = True)
Session = sessionmaker(bind=engine)



class customers(Base):
    session = Session()
    
    if session:
        print("Connection success")
    
    __tablename__ = 'customers'
    id = Column(Integer, primary_key = True)
    namaid = Column(String)
    namaLengkap = Column(String)
    email = Column(String)