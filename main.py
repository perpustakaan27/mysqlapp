from models import customers

def showUsers():
    result = customers.session.query(customers).all()
    for row in result:
        print(row.id, row.namaid, row.namaLengkap, row.email) 

def showUsersById():
    result = customers.session.query(customers).filter(customers.id==3)
    for row in result:
        print(row.id, row.namaid, row.namaLengkap, row.email)

def insertUser():
    data  = {
    "namaid" : 'RK',
    "namaLengkap" : 'Ravi Kumar',
    'email' : 'ravi@gmail.com'
    }
    customers.session.add(customers(**data))
    customers.session.commit()

def updateUsersById():
    result = customers.session.query(customers).filter(customers.id==6).one()
    result.namaid = "RaviK"
    customers.session.commit()
    print(result.id, result.namaid, result.namaLengkap, result.email)

def deleteUsersById():
    result =  customers.session.query(customers).filter(customers.id==6).one()
    customers.session.delete(result)
    customers.session.commit()
    print(result.id, result.namaid, result.namaLengkap, result.email)

if __name__ == '__main__':
    mysqldb = customers()
    showUsers()
    showUsersById()
    #insertUser()
    #updateUsersById()
    #deleteUsersById()